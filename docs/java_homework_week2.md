# 20165301 2017-2018-2 《Java程序设计》第二周学习总结

## 教材学习内容总结
#### 第二章：基本数据类型与数组
- 标识符
   
   - 第一个字符不能是数字
   - 不能是关键字
   - 不能是true、false、null
   - 字母区分大小写

- 基本数据类型

  - boolean，boolean male = true
  - int型，4个字节内存，int x = 12
  - byte型，1个字节内存，byte x = -12
  - short型，2个字节内存，short x = 12
  - long型，8个字节内存，long型常量用后缀L来表示，long x = 12L
  - char型，2个字节内存，char x = 'a'
  - float型，常量后面必须有f或F，float x = 22.76f
  - double型，double height = 23.345

- 类型转换运算

   -精度从高到低：byte short char int long float double
#### 第三章：运算符、表达式和语句
- 运算符与表达式：与c语言学习的一样
- 语句概述
   
   - if条件分支语句
   - switch开关语句
   - 循环语句
   
     - for循环
     - while循环
     - do-while循环
   - break和continue语句 

## 教材学习中的问题和解决过程

- 问题1：Scanner不理解，不知道怎么用
- 问题1解决方案：结合教材程序，边写代码边测试结果
- 问题2：byte的类型转换运算没搞懂
- 问题2解决方案：多读书


## 代码调试中的问题和解决过程


- 问题1：教材Example2_3.java不能正常运行出结果
![](https://images2018.cnblogs.com/blog/1296455/201803/1296455-20180311110710612-1387140860.png)
- 问题1解决方案：未解决
- 问题2：同问题一![](https://images2018.cnblogs.com/blog/1296455/201803/1296455-20180311123623050-423225192.png)
- 问题2解决方案：未解决
- 问题3：git push时显示的问题![](https://images2018.cnblogs.com/blog/1296455/201803/1296455-20180311133319951-353607168.png)
- 问题3解决方案：[参考网站](http://blog.csdn.net/menghuanbeike/article/details/70184787)
- 问题4：在写程序时总是会敲错一些细节。比如少了中括号，字母没拼对等等问题
- 问题4解决方案：根据系统给的提示找问题，改正并反复练习。


## [代码托管](https://gitee.com/BESTI-IS-JAVA-2018/ctf20165301_JavaProgramming)
- 代码提交过程截图
![](https://images2018.cnblogs.com/blog/1296455/201803/1296455-20180311212715809-107028218.png)

- 代码量截图
![](https://images2018.cnblogs.com/blog/1296455/201803/1296455-20180311212803308-177893352.png)


（statistics.sh脚本的运行结果截图）
![](https://images2018.cnblogs.com/blog/1296455/201803/1296455-20180311212549671-338779950.png)


## 上周考试错题总结
[参见课下作业博客](http://www.cnblogs.com/CTF5301/p/8542926.html)


## 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           | 400小时            |       |
| 第一周      | 11/11           |   1/1            | 4/4             |       |
| 第二周      | 286/297           |   2/3            | 6/10             |       |