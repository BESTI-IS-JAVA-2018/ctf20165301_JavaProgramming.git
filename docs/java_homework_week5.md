# 20165301 2017-2018-2 《Java程序设计》第五周学习总结

## 教材学习内容总结
#### 第七章：内部类与异常类
- 内部类
   - 在一个类中定义另一个类
   - 非内部类不可以是static类
- 匿名类
   - 一个子类去掉类声明后的类体
   - 用bank的一个子类（匿名类）创建对象
   ```
   new Bank(){
    匿名类的类体
   }
   ```
   - 和接口有关的匿名类
   ```
   new Computable(){
       实现接口的匿名类的类体
   }
   ```
- 异常类
  - 异常对象可以调用如下方法得到或输出有关异常信息
  ```
  public String getMessage();
  public void printStackTrace();
  public String toString();
  ```
  - try-catch语句
     - 如果在try-catch语句中执行了return语句，那么finally子语句仍然会被执行
     - try-catch语句中执行了程序退出代码，即执行System.exit(0);则不执行finally子语句
- 断言

#### 第十章：输入、输出流

- file类
   - 创建File对象的构造方法（其中filename是文件的名字，directoryPath是文件的路径，dir为一个目录）：

```
File(String filename);

File(String directoryPath,String filename);

File(File dir,String filename);
```
- 缓冲流
- 输出流：字符输入流和输出流的read和write方法使用字符数组读取数据
- 使用Scanner解析文件，用JFileChooser创建对话框




## [代码托管](https://gitee.com/BESTI-IS-JAVA-2018/ctf20165301_JavaProgramming)




（statistics.sh脚本的运行结果截图）
![](https://images2018.cnblogs.com/blog/1296455/201804/1296455-20180401123404584-814192702.png)





## 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           | 400小时            |       |
| 第一周      | 11/11           |   1/1            | 4/4             |       |
| 第二周      | 286/297           |   2/3            | 6/10             |       |
| 第三周      | 722/1004          |   1/4            | 10/20             |       |
| 第四周      | 421/1425          |   1/5            | 10/30             |       |
| 第五周      | 829/2283          |   1/6            | 10/40             |       |

