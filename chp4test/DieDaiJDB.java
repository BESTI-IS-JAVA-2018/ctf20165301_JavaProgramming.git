public class zuheshuJDB {
    public static void main(String[] args) {
            try {
               int n= Integer.parseInt(args[0]);
               int m= Integer.parseInt(args[1]);

                if (m >= 0 && n > 0 && m <= n) {
                    int[][] a = new int[n + 1][n + 1];
                    int i = 0, j = 0;
                    for (i = 0; i <= n; i++) {
                        for (j = 0; j <= i; j++) {
                            if (j > 0 && j < i) {
                                a[i][j] = a[i - 1][j - 1] + a[i - 1][j];
                            }
                            if (j == 0 || j == i) {
                                a[i][j] = 1;
                            }
                        }
                    }
                    System.out.println("C(n,m)的值为：" + a[n][m]);
                }
                else System.out.println("输入错误！（组合数n必须大于m）");

            }
            catch(Exception e) {
                System.out.println("你输入的不是数字");
            }
        
    }
}
